%global srcname kiwisolver

Name:           python-%{srcname}
Version:        1.0.1
Release:        2.1%{?dist}
Summary:        A fast implementation of the Cassowary constraint solver

License:        BSD
URL:            https://github.com/nucleic/kiwi
Source0:        https://github.com/nucleic/kiwi/archive/%{version}/%{srcname}-%{version}.tar.gz

%global _description \
Kiwi is an efficient C++ implementation of the Cassowary constraint solving \
algorithm. Kiwi is an implementation of the algorithm based on the seminal \
Cassowary paper. It is *not* a refactoring of the original C++ solver. Kiwi has \
been designed from the ground up to be lightweight and fast.

%description %{_description}


%package -n     python%{python3_pkgversion}-%{srcname}
Summary:        %{summary}
%{?python_provide:%python_provide python%{python3_pkgversion}-%{srcname}}

BuildRequires:  python%{python3_pkgversion}-devel
BuildRequires:  python%{python3_pkgversion}-setuptools
BuildRequires:  python%{python3_pkgversion}-pytest

%description -n python%{python3_pkgversion}-%{srcname} %{_description}


%prep
%autosetup -n kiwi-%{version}

# Remove bundled egg-info
rm -rf %{srcname}.egg-info


%build
%py3_build


%install
%py3_install


%check
PYTHONPATH="%{buildroot}%{python3_sitearch}" \
    py.test-%{python3_version} py/tests/


%files -n python%{python3_pkgversion}-%{srcname}
%doc README.rst
%license COPYING.txt
%{python3_sitearch}/%{srcname}.cpython-*.so
%{python3_sitearch}/%{srcname}-%{version}-py?.?.egg-info


%changelog
* Mon Sep 16 2019 Michael Thomas <michael.thomas@LIGO.ORG> - 1.0.1-2.1
- Drop python2, python34 support

* Tue Nov 13 2018 Michael Thomas <michael.thomas@LIGO.ORG> - 1.0.1-2
- Add python3.6 support

* Sat Feb 03 2018 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 1.0.1-1
- Initial package.
